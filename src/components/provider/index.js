import React from "react";

export const MyContext = React.createContext();

export const Provider = props => {

    const state = {
        name: "Frank",
        age:  29,
        cool: true,
        message: "Frank's Price Calculator",
        lands: [
            "Nederland",
            "Engeland",
            "Duitsland",
            "Frankrijk"
        ]
    };

    return (
        <MyContext.Provider value={state}>
            {props.children}
        </MyContext.Provider>
    )
};