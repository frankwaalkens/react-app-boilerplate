import React from 'react';
import PropTypes from 'prop-types';
import {MyContext} from "./../provider";

export const Select = () => (
    <select name="country" id="country">
        <MyContext.Consumer>
            {context => context.lands.map(land =>
                <option>{land}</option>
            )}
        </MyContext.Consumer>
    </select>
);

Select.propTypes = {};