import React from "react";
import {Provider} from "./provider/index";
import {Form} from "./form/index";

export const App = () => (
    <Provider>
        <Form />
    </Provider>
);